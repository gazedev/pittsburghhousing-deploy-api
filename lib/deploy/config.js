module.exports = {
  "pa_corpsearch_api": {
    "command": "bash /home/zac/pa_corpsearch_api/redeploy-prod.sh"
  },
  "pgh_geocode_api": {
    "command": "bash /home/zac/pgh_geocode_api/redeploy-prod.sh"
  },
  "pgh_owner_api": {
    "command": "bash /home/zac/pgh_owner_api/redeploy-prod.sh"
  },
  "pgh_ownersearch_api": {
    "command": "bash /home/zac/pgh_ownersearch_api/redeploy-prod.sh"
  },
  "pgh_parcels_and_addresses_api": {
    "command": "bash /home/zac/pgh_parcels_and_addresses_api/redeploy-prod.sh"
  },
  "housingdb_api": {
    "command": "bash /home/zac/pittsburgh_housing_api/redeploy-prod.sh"
  },
  "pa-eviction-cases-api": {
    "command": "bash /home/zac/pa-eviction-cases-api/redeploy-prod.sh"
  },
  "pittsburghhousing-deploy-api": {
    "command": "bash /home/zac/pittsburghhousing-deploy-api/redeploy-prod.sh"
  },
}