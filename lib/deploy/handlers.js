const deployConfig = require('./config');
const { exec } = require('child_process');

module.exports = {
  postDeploy: function (request, h) {
    console.log(request.payload);

    const projectName = request.payload.project.name;

    deployProject(projectName);

    return h
      .response({ok: true});
  },
  lib: {
    deployProject,
    deployAllProjects,
  },
};
// what does for...in do?
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in
function deployAllProjects() {
  console.log('deploying all projects');
    for (const projectName in deployConfig) {
        deployProject(projectName);
    }
}

function deployProject(projectName) {
    const command = deployConfig[projectName].command;

    var myscript = exec(command);
    myscript.stdout.on('data',function(data){
        console.log(data); // process output will be displayed here
    });
    myscript.stderr.on('data',function(data){
        console.log(data); // process error output will be displayed here
    });
}