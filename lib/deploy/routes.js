const deployHandlers = require('./handlers');

module.exports = {
  method: 'POST',
  path: '/deploy',
  handler: deployHandlers.postDeploy,
  config: {
    pre: [
      {failAction: 'error', method: (request) => {
        if (request.headers['x-gitlab-token'] !== process.env.SECRET_TOKEN) {
          throw new Boom.forbidden("Access Denied!");
        }
        return true;
      }}
    ]
  }
};