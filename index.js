const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');
require('dotenv').config();
const deployRoutes = require('./lib/deploy/routes');


module.exports = (async() => {

  const envVars = [
    'NODE_ENV',
    'SECRET_TOKEN',
    'PORT',
  ];

  for (let envVar of envVars) {
    if (!process.env[envVar]) {
      console.error(`Error: Make sure you have ${envVar} in your environment variables.`);
    }
  }

  const server = new Hapi.Server({
    port: process.env.PORT || 8084,
    routes: {
      validate: {
        failAction: async (request, h, err) => {
          if (process.env.NODE_ENV === 'production') {
            // In prod, log a limited error message and throw the default Bad Request error.
            console.error('ValidationError:', err.message);
            throw Boom.badRequest(`Invalid request payload input`);
          } else {
            // During development, respond with the full error.
            throw err;
          }
        }
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: function (request, h) {

      return h
        .response({status: 'up'});
    }
  });

  server.route(deployRoutes);

  try {
    server.start();
    console.log('server running at ' + process.env.PORT);
  } catch(err) {
    console.log(err);
  }

  return {
    server: server,
  };
  
})();
