# before you can do this, you will need git to easily get this repo
# run:
# sudo apt install git

# Clone the repo
# git clone https://gitlab.com/GazeDev/pittsburghhousing-deploy-api.git

# cd pittsburghhousing-deploy-api

# cp .env.example .env

# Fill out .env with the values of your service if needed:

# nano .env

# We are going to be installing things. Update software repositories
sudo apt-get update

# This is to remove password access to the droplet, only allowing ssh key access
sed -i 's/^PermitRootLogin yes/PermitRootLogin without-password/' /etc/ssh/sshd_config

sudo apt install nginx-full -y

# Allow access to ports 80 and 443
sudo ufw allow 'Nginx Full'

# Modify nginx.conf to listen for the right server_name if you need to

# Copy our config to the nginx sites-available directory, with a more specific name
sudo cp nginx.conf /etc/nginx/sites-available/pittsburghhousing-deploy-api

# Symlink pgh_geocode_api nginx config to sites-enabled to enable it
sudo ln -sf /etc/nginx/sites-available/pittsburghhousing-deploy-api /etc/nginx/sites-enabled

# Remove the default site
rm /etc/nginx/sites-enabled/default

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx:
sudo service nginx restart

# Let's Encrypt
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update
sudo apt install python-certbot-nginx -y

sudo certbot --nginx -d deploy.pittsburghhousing.org
# Enter email address...:
# (A)gree to Terms
# (N)o sharing of email address
# 2 - Redirect all requests to https

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx to apply the changes:
sudo service nginx restart
