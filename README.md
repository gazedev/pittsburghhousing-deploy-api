# Pittsburgh Housing Deploy Api

This api receives gitlab webhooks and uses its config to run the redeploy-prod.sh scripts in other repos on the server.

The deploy api can also update itself because it's a separate bash script that shuts down the server and restarts it, and once the server has started running the bash script, it doesn't need to stay alive for the bash script to finish.